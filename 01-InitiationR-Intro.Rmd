---
title: "Initiation à R/RStudio"
subtitle: "Introduction"
author: "GT Vallet"
output:
  learnr::tutorial:
    progressive: true
    allow_skip: true
    fig_caption: true
    language: fr
runtime: shiny_prerendered
description: "Cours d'approfondissement, M2 PICCS option R, UFR PSSSE, Université Clermont Auvergne"
licence: "CC BY-NC 4.0"
---

```{r setup, include=FALSE}
library(learnr)
library(gradethis)
knitr::opts_chunk$set(echo = T, exercise.checker = gradethis::grade_learnr, message = F, warning = F)
```

<!-- #################################################### INTRO #################################################### -->
## Avant-propos

Plus qu'un logiciel de statistiques, **R est un langage** de programmation !  

- adaptable
- modifiable
- automatisable
- omnipotent : écriture / site internet / présentation...

### Présentation

- Suite du *langage S* (propriétaire, laboratoire Bell) - 1970'
- *Langage R* à partir des années 1990 - libre et gratuit [(site de R)](https://cran.r-project.org/) :  
  - multi-plateforme
  - accessible 
  - durable
  - entraide et ressources très accessibles 
  - développé et amélioré par la communauté 
  
### Packages
R est un langage de programmation, ainsi il est possible de créer des fonctions qui peuvent accomplir une ou plusieurs tâches répétitives (avec des *paramètres*). 
Par exemple, le calcul de la moyenne est une fonction qui effectue d'abord la somme de tous les éléments donnés à la fonction (i.e. les paramètres) pour ensuite diviser cette somme par le nombre d'éléments présents.
Une fonction peut être beaucoup plus compliquée et produire par exemple un tableau de synthèse de l'ensemble des tests statistiques effectués en déterminant (et en affichant) où se situent les différences significatives.

Un ensemble de fonctions est appelé *package* (paquet ou bibliothèque). 
Ces packages peuvent être disponibles uniquement localement (sur votre ordinateur), mais l'intérêt de R est d'offrir une vaste collection de packages accessibles librement sur internet [(voir le *CRAN*)](https://cran.r-project.org/).
Certains packages sont extrêmement spécialisés, comme *`miic`* (*'Learning Causal or Non-Causal Graphical Models Using Information Theory'*), ou plus généralistes comme *`dplyr`* (*'A Grammar of Data Manipulation'*).

Voici une liste de packages recommandés dans le cadre de ce cours :

- *[`tidyr`](https://tidyr.tidyverse.org/)* : formatage/organisation des données
- *[`dplyr`](https://dplyr.tidyverse.org/)* : manipulation des données
- *[`readr`](https://readr.tidyverse.org/)* : lecture et importation de données externes dont au format Excel
- *[`psych`](http://personality-project.org/r/psych/)* : fonctions utiles pour la psychologie (pas seulement), dont les statistiques descriptives 
- *[`ggplot2`](https://ggplot2.tidyverse.org/)* : réalisation de graphiques
- *[`ggpubr`](https://rpkgs.datanovia.com/ggpubr/)* : version simplifiée de ggplot
- *[`afex`](https://github.com/singmann/afex)* : interface simplifiée pour réaliser des ANOVA
- *[`emmeans`](https://github.com/rvlenth/emmeans)* : tests post-hoc

Il est à noter qu'une majorité des fonctions présentées ici sont disponibles au sein du méta-package *[`tidyverse`](https://www.tidyverse.org/)* (regroupant `tidyr`, `dplyr`, `readr` et `ggplot2`).

**Attention**, pour utiliser les fonctions contenues au sein d'un package, il faut l'appeler avec la commande `library(nomDuPackage)`. 
Attention aussi, le nom des fonctions entre les packages peut se chevaucher (plusieurs fonctions `moyenne()` par exemple). 
Dans ce cas, ce sera le dernier package appelé qui prévaudra.


### Obtenir de l'aide
L'apprentissage d'un langage de programmation prend du temps et fonctionne par essai-erreur.
Heureusement, il existe de nombreuses ressources sur Internet pour introduire R et ses packages ou pour poser vos questions/consulter les questions déjà posées.
Voici quelques liens utiles :

- [Synthèse des ressources d'aide pour R](https://larmarange.github.io/analyse-R/ou-trouver-de-l-aide.html)
- [Aide à l'utilisation de R](https://sites.google.com/site/rgraphiques/home?authuser=0)
- [R tutorial](https://www.r-tutor.com/r-introduction)
- [Intro to R](https://monashdatafluency.github.io/r-intro-2/)
- [Programmation avec R](https://cran.r-project.org/doc/contrib/Goulet_introduction_programmation_R.pdf)
- [Ressources et actualités sur R](http://www.sthda.com/french/)
- [Exercices sous R](https://www.r-exercises.com/)

<!-- Tuto vidéo : https://www.youtube.com/playlist?list=PL6gx4Cwl9DGCzVMGCPi1kwvABu7eWv08P -->


<!-- #################################################### R & RSTUDIO #################################################### -->
## R et RStudio

### Interface R (basique)
A l'installation, R est disponible sous forme de console.
Typiquement R fonctionne à partir de 3 "fenêtres" (interfaces) :

1. *Console* (terminal) (lancement des commandes et affichage des résultats)
2. *Editeur* de texte (pour les scripts)
3. Fenêtre *graphique* (affichage des graphiques générés par R)

R peut fonctionner comme une calculatrice améliorée grâce aux opérateurs courants :

| Opérateur | Fonction                  | Exemple   |
| --------- | ------------------------- | --------- |
| +         | Addition                  | 8+4 = 12  |
| -         | Soustraction              | 18-8 = 10 |
| *         | Multiplication            | 7*3 = 21  |
| /         | Division                  | 15/3 = 5  |
| %%        | Modulo (restant division) | 15%%3 = 0 |
| ^         | Puissance                 | 3^2 = 9   |


**Application**  
Effectuer le calcul suivant dans R : (15 x 6) - (7 / 2,5)

```{r calculatrice, exercise=TRUE}
# Effectuer votre calcul ici
```

```{r calculatrice-check}
grade_result(
  pass_if(~ identical(.result, 87.2), "Bien joué !"),
  fail_if(~ TRUE, "Vérifiez votre syntaxe notamment les décimales et le signe pour multipler.")
)
```

*Attention* à bien utiliser des "**.**" au lieu des "**,**" pour marquer les décimales. Pensez aussi à remplacer le "**x**" par "**\***".

Toutefois, l'intérêt et la puissance de R résident dans sa capacité à automatiser, reproduire et transposer des opérations (fonctions), autrement dit à *programmer* avec R.
Pour ce faire, l'usage de *scripts* (fichiers textes avec l'extension ".R") permet de garder une trace des fonctions à utiliser et des ordres à exécuter.

R va pouvoir **interpréter** le contenu du fichier, c'est-à-dire exécuter les instructions écrites dans la console R, une par une, à la suite et jusqu'à la fin du fichier ou jusqu'à la survenue d'une erreur.
Il est, heureusement, possible de **commenter** son fichier texte pour pouvoir s'y retrouver ou afin de partager le document avec une tierce personne.
Dans R, les commentaires correspondent à tout élément écrit après le signe `#` que ce soit en début de ligne ou au milieu d'une ligne.

```{r, echo=T}
# Ceci est un commentaire, car la ligne commence par #
15 + 21 # commentaire en milieu de ligne, le début restera interprété par R
```

Un script R ne tient pas compte des espaces dans la rédaction du code. 
Autrement dit, `10+3` est équivalent ` 10  + 3`. 
Toutefois, il est recommandé d'être cohérent tout au long du script sur l'usage des espaces (et des indentations) afin de faciliter sa lecture et son partage.
Il existe d'ailleurs des recommandations ou des bonnes pratiques [(voir par ex. ici)](http://adv-r.had.co.nz/Style.html) ainsi que des packages qui formatent le code écrit pour le rendre conforme à un style particulier [(voir par ex. ici)](https://styler.r-lib.org/).

### Interface R (RSTUDIO)

RStudio est un *IDE* (environnement de développement intégré) regroupant les 3 interfaces de R et offrant :

- une interface simplifiée pour la gestion des *packages*, des fichiers et des sources
- une interface simplifiée pour le contrôle, l'affichage, la modification des variables
- une interface personnalisable (police, taille, couleurs, position des interfaces...)
- des fonctions supplémentaires : gestion de projets, versionning (git), interface/fonction pour la rédaction de rapport, de site internet, de présentation...

**Application**  
Présentation de l'interface de RStudio et de quelques fonctionnalités.


### Espace de travail
L'utilisation de R s'appuie sur la notion d'espace de travail, c'est-à-dire que le logiciel va définir un répertoire particulier comme référence pour importer et exporter des fichiers (des données, des graphiques, des tableaux...).
Par défaut, cet espace correspond à la racine de votre dossier utilisateur.
Vous pouvez connaître l'espace de travail actuel avec la commande `getwd()`. 

Vous pouvez spécifier manuellement un nouvel espace de travail avec la commande `setwd()`, mais il est plus simple d'utiliser la fonction proposée par RStudio dans le menu "Session" --> "Set Workgin Directory" où vous pourrez vous baser sur le répertoire du script R que vous utilisez, ou choisir un emplacement. 

**Attention**, il est important d'avoir cette notion à l'esprit pour l'import et l'export de vos données. 

RStudio propose également une gestion de projets qui permet de définir automatiquement l'espace de travail au dossier correspond. 
La gestion de projet offre d'autres avantages dont un accès simplifié aux fichiers associés au projet, le versionning avec git, etc. (voir [ici](https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects)). 


<!-- #################################################### PRINCIPES PROG #################################################### -->
## Principes de programmation

<!-- ------------------------------------------ VARIABLES ------------------------------------------ -->
### Variables

En programmation, comme en mathématique, des **variables** vont être employées pour désigner des *objets* sans avoir à les redéfinir à chaque fois.
Ainsi, *x* en mathématique peut symboliser n'importe quel nombre.
En programmation, *x* pourra non seulement représenter un nombre, mais également du texte, une fonction, un ensemble de données (tableau), une liste, etc.

Une variable est virtuellement n'importe quel objet qui peut exister dans le langage de programmation considéré.
Dans R, il existe deux manières équivalentes pour créer une variable (*assigner*), `<-` ou `=`.
La création d'une variable *y* contenant le nombre 20 pourra donc s'écrire `y <- 20` ou `y = 20`.

**Attention**, `y` et `Y` désignent deux variables différentes. 

Il est possible d'utiliser n'importe quelle suite de lettres et de nombres, avec des "." et des "-" pour créer une variable. 
Par contre, il n'est pas possible de créer une variable dont le nom débute par un chiffre ou par un caractère spécial (dont le "-"). 
Les noms suivants sont ainsi valides : "maVariable", "VaRiAblE", "V9", "data-cleaned", "dat2-Cle-2".
Il est plus simple d'utiliser des noms de variables qui soient courts, mais signifiants pour vous (évitez de tout appeler, x, y, z...).

**Attention**, R n'empêche pas de "créer" des variables (assigner une valeur) qui peuvent correspondre à des noms de variables déjà définies.
Aucun message d'avertissement ou d'erreur ne sera produit.
Par principe, ce sera toujours la valeur de la dernière assignation qui comptera.

```{r, echo=TRUE}
# Définition de "maVar" selon trois valeurs différentes
maVar = 10
maVar = "du texte"
maVar = 152.6
# Affichage de la valeur actuelle de "maVar"
maVar
```

<!-- ------------------------------------------ FONCTIONS ------------------------------------------ -->
### Fonctions
Les fonctions constituent une brique essentielle de la programmation. 
Elles permettent d'automatiser une opération (ou une série d'opérations) afin de la reproduire dans différents contextes.
Une fonction est *appelée* afin d'être utilisée.
Le plus souvent, une fonction utilisera des **paramètres**, c'est-à-dire des informations précisant le fonctionnement de la fonction ou les éléments sur lesquels travailler.

En R, une fonction est appelée en la nommant et en ajoutant des parenthèses, par ex. `moyenne()`. 
Attention, `moyenne` désignerait une variable, contenant une valeur, alors que `moyenne()` désigne la fonction *moyenne*.

La fonction moyenne a besoin d'une suite de nombres pour calculer une moyenne arithmétique, par ex. `moyenne(12, 8)` pourrait donner 10. 
Ici, 12 et 8 constituent deux paramètres de la fonction.

La création de ses propres fonctions irait au-delà des objectifs de ces ateliers. 
Toutefois, vous pourrez trouver des ressources sur la question [ici](http://www.sthda.com/french/wiki/ecrire-vos-propres-fonctions-r) ou encore [là](https://stt4230.rbind.io/programmation/fonctions_r/).

### Instructions conditionnelles
L'exécution d'un script R (et de n'importe quel programme) n'est pas nécessairement linéaire, puisque certaines parties du code pourraient n'être à évaluer que si certaines conditions s'appliquent.
Par exemple, la fonctionne `moyenne()` pourrait s'arrêter si l'utilisateur n'indique qu'une seule valeur (il n'est pas possible de calculer une moyenne sur un seul nombre).

Les principaux opérateurs permettant d'effectuer cette prise de décisions sont :

- **`if` - `else`** : *si* telle condition logique est vraie *alors* il faudra effectuer telle(s) action(s), *sinon* telle(s) autre(s) action(s). `else` est optionnel et peut se combiner avec un nouveau *if* (en `else if`) pour tester différents scénarii (si...alors, sinon si...alors, etc.)  

![Illustration if/else (www.tutorialspoint.com)](images/r_if_else_statement.jpg)

- **`switch`** : affichage d'un résultat selon une liste de choix   

![Illustration switch (www.tutorialspoint.com)](images/r_switch_statement.jpg)

Illustration de code pour un test "si, sinon si, sinon" :
```{r, echo=T}
note = 11

if( note > 18) {
  print("Excellent !")
} else if(note > 10) {
   print("Note au-dessus de la moyenne")
} else {
   print("Note en dessous de la moyenne")
}
```

Illustration de code pour un test "switch":
```{r, echo=T}
switch(3, "Choix 1", "Choix 2", "Choix 3", "Choix 4")
```

*Attention*, inverser les deux *if* pour tester si la note est supérieure à 10 sinon si elle est supérieure à 18 ne permettrait pas d'afficher le message "Excellent". 
En effet, un test *si* s'arrête à la première condition "vraie".

### Opérateurs logiques et logique booléenne

| Opérateur | Signification                                        |
| --------- | ---------------------------------------------------- |
| $==$      | test l’égalité (égal à)                              |
| $!=$      | test la différence (non égal à)                      |
| $<$       | test l’infériorité (inférieur à)                     |
| $<=$      | inférieur ou égal à                                  |
| $>$       | test la supériorité (supérieur à)                    |
| $>=$      | supérieur ou égal à                                  |
| &         | les deux conditions doivent être remplies (ET)       |
| $|$       | l’une et/ou l’autre condition doit être remplie (OU) |
| $!$       | inverse la sélection                                 |
| $\%in\%$  | test si appartient à un ensemble (dans)              |

### Boucles
En programmation, il est souvent utile de pouvoir passer par tous les niveaux d'une condition donnée ou de répéter une instruction un certain nombre de fois.
Ces deux cas de figure sont traités avec des boucles `for` où une série d'instructions sera exécutée pour chaque niveau de la boucle.
La structure dans R d'une boucle `for` est la suivante `for (i in ensemble) {instructions}`.
Voici un exemple simple pour créer une boucle qui affiche les nombres 1 à 10 :
```{r}
for (i in 1:10) {
  print(i)
}
```
La référence de l'élément en cours est appelée `i` dans cet exemple, mais n'importe quel nom peut être utilisé.
Autrement dit, `i` est une variable dont le contenu est défini par l'ensemble (ici `1:10`) de manière itérative.

Il est possible d'utiliser une boucle `for` pour indicer des ensembles.
Par exemple, imaginons un vecteur `monVect = c("un", "deux", "trois", "quatre")` :
```{r}
# Définition du vecteur
monVect = c("un", "deux", "trois", "quatre")
# Boucle for de 1 à 4 pour indicer le vecteur monVect
for (i in 1:4) {
  print( monVect[i] )
}
```

Il serait possible d'obtenir un ordre non croissant :
```{r}
# Boucle for pour les valeurs 3, 2, 4, et 1 pour indicer le vecteur monVect
for (x in c(3,2,4,1)) {
  print( monVect[x] )
}
```

Une boucle `for` n'est pas limitée à des valeurs numériques. 
Il serait ainsi possible d'effectuer directement :
```{r}
# Boucle for pour les valeurs définies auparavant pour monVect
for (monIndice in c("un", "deux", "trois", "quatre")) {
  print( monIndice )
}
```

Les boucles `for` peuvent alors permettre d'effectuer des analyses pour chaque niveau d'une variable, ou même pour le croisement de plusieurs variables (mais voir le chapitre suivant pour des fonctions plus puissantes avec `dplyr`).
```{r, eval=F}
# Exemple de croisement des niveaux de deux variables
for ( nivV1 in V1 ){   # Pour chaque niveau de V1 appelé nivV1
  for ( nivV2 in V2 ){ # Pour chaque niveau de V2 appelé nivV2
    # Instruction pour calculer la moyenne d'une performance par exemple
  }
}
```

L'autre structure classiquement utilisée en programmation pour des boucles est la fonction `while()` qui se traduit par "tant que...".
Une fonction `while` s'exécutera jusqu'à ce que la condition logique indiquée dans la fonction soit vraie.
L'utilisation de `while` est identique à celle de `for`.
```{r}
# Tant que i est inférieur à dix, afficher i
#  il est souvent nécessaire de définir un premier niveau
i=1
# Boucle while
while( i < 10 ){ 
  print(i)
  # Attention, il faut penser à incrémenter i (risque de boucle infinie)
  i = i+1 # Ajoute 1 à la valeur de i
  }
```

**Attention**, une boucle `while` mal définie pour entrainer une boucle infinie ! 
Dans ce cas, vous pouvez appuyer sur la touche `ESC` (ou Echap) ou cliquer l'icône "stop" en haut à droite de la console.
```{r eval=F}
# Exemple de boucle infinie
while(TRUE){
  print("infini")
}
```

Une boucle `while` est par exemple utilisée dans les logiciels d'expérimentation pour "capturer" la réponse des participants ("tant qu'aucune réponse n'est donnée, attendre une réponse possible").
Dans R, il pourrait s'agir de nettoyer les données tant qu'un seuil n'est pas atteint, etc.


## Les types d'objets dans R 

R opère sur différents types d'objets ayant différents *modes* (numérique, texte...), dont voici les plus fréquents :

- vecteurs (*vector*)
- matrices (*matrix*)
- tableaux (*array*)
- trames de données (*dataframe*)
- listes (*list*)
- facteurs (*factor*)
- *NULL* et *NA* (vide et donnée manquante)

<!-- ------------------------------------------ VECTEUR ------------------------------------------ -->
### Vecteurs 

Les vecteurs constituent un type d'objets parmi les plus simples de R, mais surtout parmi les plus utiles.
Un vecteur se définit par un même type de données, c'est-à-dire que tous les éléments qui composent un vecteur devront avoir le même *mode* (équivalent au plus petit dénominateur commun) entre des nombres (entiers, complexes...), du texte et des valeurs booléennes (vrai/faux).
Le plus petit dénominateur sera du texte si au moins un des éléments est du texte.

Un vecteur est créé par la **concaténation** d'éléments, abrégé `c()`, par exemple `c(3, 8, 5)` est un vecteur contenant les valeurs 3, 8 et 5.

**Application**  
Créer un vecteur contenant les éléments suivants : 6, 12, 8, 11, 6 :

```{r vectNb, exercise=TRUE}
#Taper votre code ici
```

```{r vectNb-solution}
c(6,12,8,11,6)
```

```{r vectNb-check}
grade_code("Très bien !")
```

Créer un vecteur contenant les éléments suivants : un, 4, deux, 5 :

```{r vectMixte, exercise=TRUE}
#Taper votre code ici
```

```{r vectMixte-solution}
c("un",4,"deux",5)
```

```{r vectMixte-check}
grade_code("Très bien !")
```

**Attention**, pour insérer `un` dans le vecteur, il faut ajouter des guillemets, autrement R cherche la variable `un` qui n'a pas été définie.
Voici un exemple avec "un" défini comme une variable.

```{r, echo=T}
# Assignation de la variable "un"
un = "mon texte qui vaut un"

# Création du vecteur
c(un, 4, "deux", 6)
```


**Manipulation des vecteurs**  
Il est possible d'effectuer beaucoup d'opérations sur des vecteurs (application de fonctions comme la moyenne, la somme, l'écart-type, etc.), mais il est également possible d'agir directement sur un vecteur pour le construire ou pour l'organiser.

Voici quelques-unes des fonctions utiles :  

- `seq` : génère une séquence selon une règle pré-déterminée, par ex. `seq(1,10,1)` (ou `seq(from=1,to=10,by=1)`) donnera "`r seq(1,10,1)`". Pour une séquence décroissante, il faudra ajouter le signe "-" devant le nombre spécifié dans `by` (`seq(10,1,-1)`).
- `rep` : répète un item un nombre de fois prédéfini, par ex. `rep("bouh",10)` affichera "`r rep("bouh",10)`". Il est possible de créer des combinaisons plus complexes, par ex. `rep(c("haut", "bas"), each=3)` donnera "`r rep(c("haut", "bas"), each=3)`".
- `sort` : tri les éléments du vecteur en ordre croissant ou décroissant, par ex. `sort(c(5,2,7,1))` donnera `r sort(c(5,2,7,1))` et  `sort(c(5,2,7,1), decreasing=T)` donnera "`r sort(c(5,2,7,1), decreasing=T)`".
- `rank` : affiche le rang de chacun des éléments du vecteur, par ex. `rank(c(5,2,7,1))` donnera "`r rank(c(5,2,7,1))`".
- `rev` : inverse l'ordre des éléments du vecteur, par ex. `rev(c("oui","peut-être","non","NSP"))` donnera "`r rev(c("oui","peut-être","non","NSP"))`"

**Application**  
Créer rapidement un vecteur contenant les éléments suivants : 30, 25, 20, 15, 10, 5, 0 : 

```{r vectSeq, exercise=TRUE}
#Taper votre code ici
```

```{r vectSeq-solution}
seq(30, 0, -5)
```

```{r vectSeq-check}
grade_code("Très bien !")
```

Utiliser `rev` et `sort` pour afficher de manière décroissante les nombres suivants : 253, 286, 249, 237, 251, 264  

```{r revSort, exercise=TRUE}
#Taper votre code ici
```

```{r revSort-solution}
rev(sort(c(253, 286, 249, 237, 251, 264)))
```

```{r revSort-check}
grade_code("Très bien !")
```

**Attention**, il faut regrouper les nombres dans un vecteur, car la fonction `sort()` opère sur un seul paramètre, le paramètre suivant sert à spécifier s'il faut aller en ordre décroissant. D'ailleurs, le même résultat aurait pu être obtenu avec le code suivant `sort(c(253, 286, 249, 237, 251, 264), decreasing = TRUE)`. Toutefois, cet exemple illustre le fait que des fonctions puissent s'emboiter les unes dans les autres.


<!-- ------------------------------------------DATA FRAME ------------------------------------------ -->
### Dataframes
Après les vecteurs, le principal objet R que vous serez amené à manipuler est certainement la trame de données (*data frame*) qui correspond grossièrement à un tableur (type Excel).
Les dataframes sont organisés sous forme de tableaux (lignes et colonnes), tout comme les matrices. 
La principale différence entre ces deux objets est qu'une matrice ne doit être composée que de nombres, alors que le dataframe peut être constitué de tous les types (et donc modes) de données.
Le dataframe permet d'utiliser des noms de lignes et des noms de colonnes (typiquement le nom des variables), contrairement aux matrices.

Exemple d'une matrice
```{r echo=F}
set.seed(7752)
```

```{r}
# Création d'une matrice
round(matrix(rexp(21, rate=.1), ncol=3))
```

Exemple d'un dataframe
```{r}
# Création d'un dataframe
data.frame(
  Identifiant = paste("P", 1:10, sep=""),
  Age = sample(x = c("Jeunes", "Âgés"), size = 10, replace = T),
  Performance = sample(x = c(9:18), size = 10, replace = T),
  Accord = sample(x = c(TRUE, FALSE), size = 10, replace = T)
 )
```


## Indiçage et sous-ensembles
L'indiçage consiste à sélectionner qu'une partie d'un objet R, par exemple, le 3e élément d'un vecteur, ou toutes les valeurs supérieures à 10, etc.
L'indiçage est représenté par des `[]` après le nom de l'objet.

### Indiçage numérique
L'indiçage numérique consiste à spécifier quel(s) élément(s) doivent être sélectionnés ou ignorés selon leur position.
Pour un vecteur, `vect[2]` sélectionnera le 2e élément présent dans `vect` alors que `vect[-2]` sélectionnera tous les éléments sauf le 2e.

Bien entendu, il est possible de sélectionner plusieurs éléments en spécifiant un vecteur ou une plage d'indices (comme pour un tableur à l'aide du signe `:`) :
```{r, echo=T}
# Création d'un vecteur de 10 éléments
vect = seq(10, 100, 10) # équivalent à c(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
# Sélection du 5e élément
vect[5]
# Sélection de tous les éléments sauf le 8e
vect[-8]
# Sélection du 3e, 7e et 10e éléments
vect[c(3, 7, 10)]
# Sélection du 4e au 6e éléments inclus
vect[4:6]
```

Il est aussi possible d'effectuer un tri selon une ou plusieurs conditions logiques :
```{r}
vect > 70       # ce code effectue le test logique valeurs > à 70
vect[vect > 70] # ce code sélectionne les valeurs > 70 du vecteur

vect[vect < 30 | vect > 70] # ce code sélectionne les valeurs inférieures à 30 OU celles supérieures à 70
```

Cette logique peut s'appliquer pour du texte (mais peu utile pour un vecteur) :
```{r}
color = c("bleu", "orange", "vert", "rouge", "violet") 
color[color=="bleu" | color=="vert"]
```

<!-- Application dataframe + ref pour creuser fonctions associer -->

Transposer à un dataframe, il s'agira de différencier les lignes des colonnes. 
Pour ce faire, R considère que le premier nombre désignera les lignes alors que le second nombre correspondra aux colonnes.
Pour afficher une ligne entière, il suffira de laisser vide le deuxième nombre `dataframe[X, ]` et vice versa pour afficher une colonne entière `dataframe[ , Y]`.
La récupération d'un croisement d'une ligne et d'une colonne s'effectue en indiquant le numéro de ligne et celui de la colonne ciblée `dataframe[X, Y]`.

```{r}
# Création d'un dataframe appelé "mydataframe"
mydataframe =data.frame(
  Identifiant = paste("P", 1:10, sep=""),
  Age = sample(x = c("Jeunes", "Âgés"), size = 10, replace = T),
  Performance = sample(x = c(9:18), size = 10, replace = T),
  Accord = sample(x = c(TRUE, FALSE), size = 10, replace = T)
 )

# Affichage de "mydataframe"
mydataframe
```

```{r}
# Affichage de la seconde ligne
mydataframe[2,]

# Affichage de la deuxième colonne
mydataframe[,2]

# Affichage de l'intersection de la 5e ligne et de la troisième colonne (cellule d'un tableur)
mydataframe[5,3]

# Utilisation d'un vecteur pour sélectionner la 2e, 6e et 8e lignes
mydataframe[c(2,6,8),]

# Utilisation d'une plage d'indiçage 6e à 8e lignes pour le 3e et 4e colonnes
mydataframe[6:8,3:4]
```

Il est également possible de sélectionner une colonne par son nom, en utilisant un `$` entre le nom du dataframe et le nom de la colonne : `nomDataFrame$NomColomne`.
*Astuce* : utiliser la touche `tabulation` pour lister les objets disponibles à partir d'un début de texte et compléter automatiquement le nom souhaité.
```{r}
# Affichage de la deuxième colonne
mydataframe[,2]
# Equivaut à 
mydataframe$Age
```

Là encore, il est possible de sélectionner un sous-ensemble du dataframe avec des conditions logiques, numériques ou textuelles.
```{r, echo=T}
# Sélectionner uniquement les lignes où les personnes ont une performance supérieure à 10
mydataframe[ mydataframe$Performance > 10, ]

# Sélectionner que les jeunes adultes
mydataframe[mydataframe$Age == "Jeunes",]

# Sélectionner uniquement les personnes âgées ayant données leur accord et ayant moins de 18 comme performance
mydataframe[mydataframe$Age == "Âgés" & mydataframe$Accord == T & mydataframe$Performance < 18 ,]
```

### Subset et select
Il est possible de créer un sous-ensemble d'un dataframe plus facilement avec la fonction `subset()` et de ne garder que les colonnes souhaitées avec `select()`.
```{r, echo=T}
# mydataframe[mydataframe$Age == "Âgés" & mydataframe$Accord == T & mydataframe$Performance < 18 ,] équivaut à
subset(mydataframe, 
       subset = 
         Age == "Âgés" &
         Accord == T &
         Performance < 18
)
# Ne garder que la colonne Performance
subset(mydataframe, 
       subset = 
         Age == "Âgés" &
         Accord == T &
         Performance < 18,
       select = Performance
)
```


### Filter
La fonction subset est intéressante, mais la fonction la plus puissante reste la fonction `filter()` proposée dans le package `dplyr` qui peut se combiner avec la fonction `select()` du même package.

```{r echo=T}
# Appel du package dplyr
library(dplyr)
# mydataframe[mydataframe$Age == "Âgés" & mydataframe$Accord == T & mydataframe$Performance < 18 ,] équivaut à
filter(mydataframe, 
          Age =="Âgés", Accord == T, Performance < 18)
# Combinaison de filter et de select selon le principe d'enchainement de dplyr %>%
mydataframe %>% 
  filter(Age == "Âgés", Accord == T, Performance < 18) %>% 
  select(Performance)
```


### Pour aller plus loin
- [Quelques fonctions supplémentaires pour manipuler vos données](https://statistique-et-logiciel-r.com/8-fonctions-manipulation/)
- [Liste des verbes pour sélectionner "intelligemment" des colonnes](https://dplyr.tidyverse.org/reference/select.html)
