Cours d'introduction à R et RStudio (M2 PICCS option R -- UCA)
========================================================
author: Guillaume Vallet
date: 15/12/2021
autosize: true


Qui suis-je ?
========================================================
incremental: true

- MCF-HDR en psychologie (spé psy cognitive et neuropsychologie)
- Psychologue, pas mathématicien ou statisticien ou informaticien
- Formation "classique" en statistiques
- Formation à SPSS
- **Autoformation** à Matlab, Python et R


Qui êtes-vous ?
========================================================

![](src/question.jpg)


Pour mieux cibler la formation
========================================================

**https://www.wooclap.com/MBSQXH**

![](src/sondage.jpg)


Avertissements
========================================================
incremental: true

- *"steep learning curve"*
- Persévérance !
- Patience !


Quelques conseils
========================================================
incremental: true

- Savoir chercher de l'aide / S'entraider
- Apprendre à apprendre vs. apprendre à résoudre des problèmes
- Se forcer à utiliser R
- Commencer petit, avancer un pas à la fois
- Adapter ce qui existe 
- Garder une trace


Démonstrations
========================================================

![](src/demo.jpg)


Programme
========================================================
incremental: true

1. Introduction à R/RStudio
2. Statistiques descriptives
3. Liens entre des variables (corrélation/régression)
4. Différence entre des variables (test *t*, ANOVA...)


Fonctionnement
========================================================
incremental: true

- Semi-guidé
- Tutoriels interactifs
- Ateliers --> mise en pratique

Ateliers
========================================================

https://gitlab.com/gtvallet/ateliers-r


Iconographie
========================================================

- Freepik
- slidesgo / Freepik
- makyzz / Freepik

